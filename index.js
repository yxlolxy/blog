if ('serviceWorker' in navigator) {
  navigator.serviceWorker
    .register('/sw.js', { scope: '/' })
    .then((res) => {
      if (res.installing) {
        console.log('Service worker installing!')
      } else if (res.waiting) {
        console.log('Service worker installed!')
      } else if (res.active) {
        console.log('Service worker active!')
      }
    }).catch(err => console.log(err || 'Service worker error'))
}