function isPC() {
    const ua = window.navigator.userAgent
    if (/Windows NT/.test(ua)) {
        return !/Nokia/.test(ua)
    } else if (/Mac OS X/.test(ua) && !/like Mac OS X/.test(ua)) return true
    else if (/(Linux|Unix|X11)/.test(ua)) {
        return !/Mobile/.test(ua)
    } else return false
}

document.addEventListener('DOMContentLoaded',function(){
    if (isPC()) {
        let el = document.querySelector('#live2d ')
        if (!el) {
            el = document.createElement('canvas')
            el.id = "live2d"
            el.width = "250"
            el.height = "250"
            el.style.position="fixed"
            el.style.bottom="15px"
            el.style.left="15px"
            el.style.zIndex="9999"
            document.body.appendChild(el)
        }
        loadlive2d("live2d", "https://cdn.jsdelivr.net/gh/yxlolxy/live2d/model/hijiki/hijiki.model.json");
    }
});