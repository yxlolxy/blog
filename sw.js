const whiteList = [
  'live2d',
  'free-cdn',
  'favicon.ico',
  'unpkg',
  'jsdelivr',
]
const isWhiteUrl = url => {
  for(const item of whiteList) {
    if (url.includes(item)) return true
  }
  return false
}
addEventListener('fetch', function (event) {
    if (event.request.method === 'GET' && isWhiteUrl(event.request.url)) {
      event.respondWith(
        caches.match(event.request).then(function (res) {
          if (res !== undefined) {
            return res
          } else {
            return fetch(event.request)
              .then(function (res) {
                console.log(event.request.url + 'is cached!')
                let resClone = res.clone()
                caches.open('yxlolxy_cache').then(function (cache) {
                  cache.put(event.request, resClone)
                })
                return res
              })
              .catch((err) => Promise.reject(err))
          }
        })
      )
    }
})
